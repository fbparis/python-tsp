#!/usr/bin/python
# -*- coding: utf-8 -*-

# This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/.

import numpy as np
import scipy.spatial as spt
import sys
import tsplib95
from random import sample

def tsp(nodeCount, points, opt=None):
	"""
	Initialize a solution, improve it, graph it and return it.
	"""
	from plot import showpath
	if opt:
		sys.stderr.write("optimal solution should be around %f\n" % opt)
	sys.stderr.write("computing distance matrix for nodeCount=%d..." % nodeCount)
	D = spt.distance.squareform(spt.distance.pdist(np.array(points), "euclidean"))
	sys.stderr.write("done\n")
	solution = random_insertions(nodeCount, D, -1)
	solution = best_insertions(nodeCount, D, solution)
	score = total_length(solution, D)
	showpath([points[x] for x in solution])
	return score, solution

def total_length(solution, D):
	"""
	Return the total distance (score) of a tour
	"""
	return sum([D[solution[x-1], solution[x]] for x in xrange(len(solution))])

def random_insertions(nodeCount, D, max_loops=1):
	"""
	Generate a solution using best insertions in random order.
	Set max_loops to:
		* -1	: searching for the best solution until KeyboardInterrupt
		* 0		: returning a random solution (a bad one)
		* n > 0	: looping n times
	"""
	from random import shuffle
	sys.stderr.write("starting random_insertions for nodeCount=%d and max_loops=%d\n" % (nodeCount, max_loops)) 
	bestScore = None
	nodes = range(nodeCount)
	loop = 0
	while 1:
		try:
			shuffle(nodes)
			if (max_loops >= 0) and (loop == max_loops):
				break
			loop += 1
			solution = nodes[:3]
			solutionScore = total_length(solution, D)
			for i in nodes[3:]:
				# insert i in the tour
				R = D[solution[-1:]+solution[:-1],i] + D[solution,i] - D[solution,solution[-1:]+solution[:-1]]
				solutionScore += np.amin(R)
				if (bestScore is not None) and (solutionScore > bestScore):
					break
				solution.insert(np.argmin(R), i)
			if (bestScore is None) or (solutionScore < bestScore):
				solution = fast_swaps(nodeCount, D, solution)
				solutionScore = total_length(solution, D)
				sys.stderr.write("loop=%d: best score is now %f\n" % (loop, solutionScore))
				bestScore, best = solutionScore - 1e-8, list(solution)
		except KeyboardInterrupt:
			sys.stderr.write("%d loops done\n" % loop)
			break
	if bestScore is not None:
		return best
	return nodes

def best_insertions(nodeCount, D, best=None):
	"""
	Produce a good / optimal tour pretty quickly.
	Can be used to improve an existing tour or create a new one inserting cities in the tour in random order.
	Insertions in a tour are always made to minimize the cost function (total tour length).
	On first and last round, the solution is passed to another algorithm which will essentially try to swap some cities and remove any crossing edges.
	Search for a better tour then works as follow:
		- starting from n=1, remove n random cities from the tour and reinsert them
		- if the tour is improved, restart from n=1, else n=n+1
		- if n==nodeCount/2 then restart from n=1
	"""
	sys.stderr.write("starting best_insertions for nodeCount=%d\n" % (nodeCount))
	if best is None:
		best = range(nodeCount)
	bestScore = total_length(best, D)
	sys.stderr.write("initial score is %f\n" % bestScore)
	loop, n, nMax, swapNodes = 0, nodeCount, nodeCount * 0.8, True
	while 1:
		try:
			loop += 1
			nodes = sample(best, n)
			if n > nMax:
				m = max(0, 3 - (nodeCount - n))
				solution = [x for x in best if x not in nodes] + nodes[:m]
			else:
				solution = [x for x in best if x not in nodes]
				m = 0
			solutionScore = total_length(solution, D)
			for i in nodes[m:]:
				# insert i in the tour
				R = D[solution[-1:]+solution[:-1],i] + D[solution,i] - D[solution,solution[-1:]+solution[:-1]]
				solutionScore += np.amin(R)
				if solutionScore > bestScore:
					break
				solution.insert(np.argmin(R), i)
			if solutionScore < bestScore:
				sys.stderr.write("loop=%d, n=%d: best score is now %f\n" % (loop, n, solutionScore))
				bestScore, best, n, swapNodes = solutionScore - 1e-8, list(solution), 0, True
			if n >= nMax:
				if swapNodes:
					swapNodes = False
					solution = fast_swaps(nodeCount, D, best)
					solutionScore = total_length(solution, D)
					if solutionScore < bestScore:
						sys.stderr.write("loop=%d, n=%d: best score is now %f\n" % (loop, n, solutionScore))
						bestScore, best = solutionScore - 1e-8, list(solution)
				n = 0
			n += 1
		except KeyboardInterrupt:
			sys.stderr.write("stopped on loop %d, bestScore=%f\n" % (loop, bestScore))
			if swapNodes:
				best = fast_swaps(nodeCount, D, best)
			break
	return best

def fast_swaps(nodeCount, D, solution=None):
	"""
	Improve solution using path swaps.
	"""
	if solution is None:
		solution = range(nodeCount)
	bestScore = total_length(solution, D)
	sys.stderr.write("starting fast_swaps for nodeCount=%d and bestScore=%f\n" % (nodeCount, bestScore))
	loop = i = score = 0
	while 1:
		try:
			loop += 1
			i0 = max(i-1, 0)
			S1a = D[solution[:-1], solution[-1:]+solution[:-2]]
			S2a = D[solution[1:], solution[2:]+solution[:1]]
			for i in xrange(i0, nodeCount-1):
				A = S1a[i] + S2a[i:]
				S1b = D[solution[i-1], solution[i+1:]]
				S2b = D[solution[i], solution[i+2:]+solution[:1]]
				B = S1b + S2b
				R = (A - B)[:nodeCount - i - 3 + min(2, i)]
				score = np.amax(R)
				if score > 0:
					j = i + 1 + np.argmax(R)
					solution[i:j+1] = solution[i:j+1][::-1]
					bestScore -= score
					# sys.stderr.write("loop %d: swapping from %d to %d, new best score is %f\n" % (loop, i, j, bestScore))
					break
			if score <= 0:
				if i0 == 0:
					break
				else:
					i = 0
		except KeyboardInterrupt:
			break
	sys.stderr.write("%d loops done, bestScore=%f\n" % (loop, bestScore))
	return solution

if __name__ == '__main__':
	if len(sys.argv) > 1:
		name = sys.argv[1].strip()
	else:
		name = sample([x[0] for x in tsplib95.instances if x[1] < 5000], 1)[0]
		sys.stderr.write("random instance chosen: %s\n" % name)
	nodeCount, points, opt = tsplib95.load(name)
	print tsp(nodeCount, points, opt)