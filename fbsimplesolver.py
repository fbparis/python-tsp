#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys, tsplib95
import numpy as np
import scipy.spatial as spt
from random import sample
from plot import showpath

def total_length(solution, D):
	"""
	Return the total distance (score) of a tour
	"""
	return sum([D[solution[x-1], solution[x]] for x in xrange(len(solution))])

def best_insertions(nodeCount, D):
	"""
	Produce a good / optimal tour pretty quickly.
	Insertions in a tour are always made to minimize the cost function (total tour length).
	Search for a better tour works as follow:
		- pick a random n in [1, nodeCount*0.8], remove n cities from tour and reinsert them
	"""
	sys.stderr.write("starting best_insertions for nodeCount=%d\n" % (nodeCount))
	best = fast_swaps(nodeCount, D, range(nodeCount))
	bestScore = total_length(best, D)
	sys.stderr.write("initial score is %f\n" % bestScore)
	loop, n = 0, nodeCount - 1
	while 1:
		try:
			loop += 1
			nodes = sample(best, n)
			solution = [x for x in best if x not in nodes]
			solutionScore = total_length(solution, D)
			for i in nodes:
				# insert i in the tour
				R = D[solution[-1:]+solution[:-1],i] + D[solution,i] - D[solution,solution[-1:]+solution[:-1]]
				solutionScore += np.amin(R)
				if solutionScore > bestScore:
					break
				solution.insert(np.argmin(R), i)
			if solutionScore < bestScore:
				sys.stderr.write("loop=%d, n=%d: best score is now %f\n" % (loop, n, solutionScore))
				bestScore, best = solutionScore - 1e-8, list(solution)
			else:
				n = min(sample(xrange(1, int(nodeCount * 0.9)), 3))
		except KeyboardInterrupt:
			sys.stderr.write("stopped on loop %d, bestScore=%f\n" % (loop, bestScore))
			best = fast_swaps(nodeCount, D, best)
			break
	return best

def fast_swaps(nodeCount, D, solution=None):
	"""
	Improve solution using path swaps.
	"""
	if solution is None:
		solution = range(nodeCount)
	bestScore = total_length(solution, D)
	sys.stderr.write("starting fast_swaps for nodeCount=%d and bestScore=%f\n" % (nodeCount, bestScore))
	loop = i = score = 0
	while 1:
		try:
			loop += 1
			i0 = max(i-1, 0)
			S1a = D[solution[:-1], solution[-1:]+solution[:-2]]
			S2a = D[solution[1:], solution[2:]+solution[:1]]
			for i in xrange(i0, nodeCount-1):
				A = S1a[i] + S2a[i:]
				S1b = D[solution[i-1], solution[i+1:]]
				S2b = D[solution[i], solution[i+2:]+solution[:1]]
				B = S1b + S2b
				R = (A - B)[:nodeCount - i - 3 + min(2, i)]
				score = np.amax(R)
				if score > 0:
					j = i + 1 + np.argmax(R)
					solution[i:j+1] = solution[i:j+1][::-1]
					bestScore -= score
					# sys.stderr.write("loop %d: swapping from %d to %d, new best score is %f\n" % (loop, i, j, bestScore))
					break
			if score <= 0:
				if i0 == 0:
					break
				else:
					i = 0
		except KeyboardInterrupt:
			break
	sys.stderr.write("%d loops done, bestScore=%f\n" % (loop, bestScore))
	return solution

if __name__ == '__main__':
	if len(sys.argv) > 1:
		name = sys.argv[1].strip()
	else:
		name = sample([x[0] for x in tsplib95.instances if x[1] < 5000], 1)[0]
		sys.stderr.write("random instance chosen: %s\n" % name)
	nodeCount, points, opt = tsplib95.load(name)
	sys.stderr.write("optimal solution should be around %f\n" % opt)
	sys.stderr.write("computing distance matrix for nodeCount=%d..." % nodeCount)
	D = spt.distance.squareform(spt.distance.pdist(np.array(points), "euclidean"))
	sys.stderr.write("done\n")
	solution = best_insertions(nodeCount, D)
	score = total_length(solution, D)
	showpath([points[x] for x in solution])
	print score, solution
