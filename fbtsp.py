#!/usr/bin/python
# -*- coding: utf-8 -*-

# This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/.

import sys, math

cachedDistances = dict()

def tsp(nodeCount, points):
	"""
	Initialize a solution, improve it, graph it and return it.
	"""
	from plot import showpath
	global length
	if nodeCount > 5000:
		length = distance
	else:
		length = distance_cached
	sys.stderr.write("using %s to compute distances\n" % length.__name__)
	score, solution = best_insertions(nodeCount, points)
	score, solution = multi_swaps(nodeCount, points, score, solution)
	showpath([points[x] for x in solution])
	return score, solution

def benchmark(nodeCount, points, score, solution, algos):
	"""
	Compare several algos and return the best one
	"""
	from operator import itemgetter
	from time import time
	results = []
	for f in algos:
		start = time()
		results.append((f(nodeCount, points, score, list(solution)), f.__name__, time()-start))
	results.sort(key=itemgetter(0))
	for (score, solution), name, duration in results:
		sys.stderr.write("*** %s: %f (%f seconds)\n" % (name, score, duration))
	return results[0][0]

def distance_cached(point1, point2):
	"""
	Return euclidian distance between two points.
	"""
	global cachedDistances
	if point1 > point2:
		point1, point2 = point2, point1
	if (point1, point2) not in cachedDistances:
		cachedDistances[(point1, point2)] = distance(point1, point2)
	return cachedDistances[(point1, point2)]
def distance(point1, point2):
	"""
	Return euclidian distance between two points.
	"""
	return math.sqrt((point1[0] - point2[0])**2 + (point1[1] - point2[1])**2)

def random_insertions(nodeCount, points, max_loops=1):
	"""
	Generate a solution using best insertions in random order.
	Set max_loops to:
		* -1	: searching for the best solution until KeyboardInterrupt
		* 0		: returning a random solution (a bad one)
		* n > 0	: looping n times
	"""
	from random import shuffle
	sys.stderr.write("starting random_insertions for nodeCount=%d and max_loops=%d\n" % (nodeCount, max_loops)) 
	bestScore = None
	nodes = range(nodeCount)
	loop = 0
	while 1:
		try:
			shuffle(nodes)
			if (max_loops >= 0) and (loop == max_loops):
				break
			loop += 1
			solution = nodes[:3]
			solutionScore = sum([length(points[solution[x]], points[solution[x-1]]) for x in xrange(3)])
			for i in nodes[3:]:
				# insert i in the tour
				bs = None
				for j in xrange(len(solution)):
					score = -length(points[solution[j-1]], points[solution[j]]) + length(points[solution[j-1]], points[i]) + length(points[i], points[solution[j]])
					if (bs is None) or (score < bs):
						bs = score
						b = j
				solutionScore += bs
				if (bestScore is not None) and (solutionScore > bestScore):
					break
				solution.insert(b, i)
			if (bestScore is None) or (solutionScore < bestScore):
				bestScore, best = solutionScore, list(solution)
				sys.stderr.write("best score after %d loops is %f\n" % (loop, bestScore))
		except KeyboardInterrupt:
			sys.stderr.write("%d loops done\n" % loop)
			break
	if bestScore is not None:
		return bestScore, best
	return sum([length(points[nodes[x-1]], points[nodes[x]]) for x in xrange(nodeCount)]), nodes
def nearest_neighbour(nodeCount, points):
	"""
	Generate a solution using nearest neighbour in random order.
	"""
	from random import shuffle
	sys.stderr.write("starting nearest_neighbour for nodeCount=%d\n" % (nodeCount)) 
	nodes = range(nodeCount)
	shuffle(nodes)
	bestScore = None
	for start in nodes:
		try:
			score, solution = 0.0, [start]
			remain = set([x for x in nodes if x != start])
			first = last = points[start]
			while len(remain) > 0:
				bs = None
				for i in remain:
					s = length(last, points[i])
					if (bs is None) or (s < bs):
						bs = s
						b = i
				solution.append(b)
				score += bs
				last = points[b]
				remain.remove(b)
			score += length(last, first)
			if (bestScore is None) or (score < bestScore):
				bestScore, best = score, list(solution)
				sys.stderr.write("new best score is %f\n" % bestScore)
		except KeyboardInterrupt:
			break
	return bestScore, best

def multi_swaps(nodeCount, points, bestScore, solution):
	"""
	Alias for the best implementation. Also make sure the score is correct.
	v5 / v4, v2
	"""
	score, solution = benchmark(nodeCount, points, bestScore, solution, [multi_swaps_v5, multi_swaps_v4])
	# score, solution = multi_swaps_v4(nodeCount, points, bestScore, solution)
	return sum([length(points[solution[x-1]], points[solution[x]]) for x in xrange(nodeCount)]), solution
def multi_swaps_v5(nodeCount, points, bestScore, solution):
	"""
	Improve solution using path fast swaps and fast reinsertions.
	"""
	sys.stderr.write("starting multi_swaps_v5 for nodeCount=%d and bestScore=%f\n" % (nodeCount, bestScore))
	loop = 0
	while 1:
		try:
			loop += 1
			score = bestScore
			bestScore, solution = fast_swaps(nodeCount, points, bestScore, solution)
			bestScore, solution = fast_reinsertions(nodeCount, points, bestScore, solution)
			if score == bestScore:
				break
			sys.stderr.write("loop %d: best score is now %f\n" % (loop, bestScore))
		except KeyboardInterrupt:
			sys.stderr.write("%d loops done\n" % loop)
			break
	return bestScore, solution
def multi_swaps_v4(nodeCount, points, bestScore, solution):
	"""
	Improve solution using path swaps and reinsertions.
	"""
	sys.stderr.write("starting multi_swaps_v4 for nodeCount=%d and bestScore=%f\n" % (nodeCount, bestScore))
	bestScore, solution = fast_swaps(nodeCount, points, bestScore, solution)
	loop = i = fail = 0
	while 1:
		try:
			loop += 1
			# Testing reinsertion method
			while 1:
				bs = None
				D = [length(points[solution[x-1]], points[solution[x]]) for x in xrange(nodeCount)]
				for i in xrange(nodeCount):
					pi = points[solution[i]]
					exclude = set([i, (i+1) % nodeCount, (nodeCount+i+1) % nodeCount])
					d = D[i] + D[(i+1) % nodeCount] - length(points[solution[i-1]], points[solution[(i+1) % nodeCount]])
					for j in [x for x in xrange(nodeCount) if x not in exclude]:
						score = length(pi, points[solution[j]]) + length(pi, points[solution[j-1]]) - D[j]
						if score < d:
							score = d - score
							if (bs is None) or (score > bs):
								bs = score
								b = (i,j)
				if bs is None:
					fail += 1
					break
				fail = 0
				i, j = b
				s = solution.pop(i)
				if i < j:
					solution.insert(j-1, s)
				else:
					solution.insert(j, s)
				bestScore -= bs
				sys.stderr.write("loop %d: swapping from %d to %d with method 2, new best score is %f\n" % (loop, i, j, bestScore))
			if fail == 2:
				break
			# Testing swaps
			while 1:
				bs = None
				for i in xrange(0, nodeCount-1):
					s1 = length(points[solution[i-1]], points[solution[i]])
					for j in xrange(i+1, nodeCount-2+min(2, i)):
						s2 = s1 + length(points[solution[j]], points[solution[(j+1) % nodeCount]])
						solution[i], solution[j] = solution[j], solution[i]
						score = s2 - (length(points[solution[i-1]], points[solution[i]]) + length(points[solution[j]], points[solution[(j+1) % nodeCount]]))
						solution[i], solution[j] = solution[j], solution[i]
						if score > 0:
							if (bs is None) or (score > bs):
								bs = score
								b = (i,j)
				if bs is None:
					fail += 1
					break
				fail = 0
				i,j = b
				solution[i:j+1] = solution[i:j+1][::-1]
				bestScore -= bs
				sys.stderr.write("loop %d: swapping from %d to %d with method 0, new best score is %f\n" % (loop, i, j, bestScore))
			if fail == 2:
				break
		except KeyboardInterrupt:
			sys.stderr.write("%d loops done\n" % loop)
			break
	bestScore = sum([length(points[solution[x-1]], points[solution[x]]) for x in xrange(nodeCount)])
	return bestScore, solution
def multi_swaps_v2(nodeCount, points, bestScore, solution):
	"""
	Improve solution using path swaps and reinsertions.
	"""
	sys.stderr.write("starting multi_swaps_v2 for nodeCount=%d and bestScore=%f\n" % (nodeCount, bestScore))
	bestScore, solution = fast_swaps(nodeCount, points, bestScore, solution)
	loop = i = 0
	while 1:
		try:
			loop += 1
			bs = None
			# Testing reinsertion method
			D = [length(points[solution[x-1]], points[solution[x]]) for x in xrange(nodeCount)]
			for i in xrange(nodeCount):
				pi = points[solution[i]]
				exclude = set([i, (i+1) % nodeCount, (nodeCount+i+1) % nodeCount])
				d = D[i] + D[(i+1) % nodeCount] - length(points[solution[i-1]], points[solution[(i+1) % nodeCount]])
				for j in [x for x in xrange(nodeCount) if x not in exclude]:
					score = length(pi, points[solution[j]]) + length(pi, points[solution[j-1]]) - D[j]
					if score < d:
						score = d - score
						if (bs is None) or (score > bs):
							bs = score
							b = (2,i,j)
			# Testing swaps
			for i in xrange(0, nodeCount-1):
				s1 = length(points[solution[i-1]], points[solution[i]])
				for j in xrange(i+1, nodeCount-2+min(2, i)):
					s2 = s1 + length(points[solution[j]], points[solution[(j+1) % nodeCount]])
					solution[i], solution[j] = solution[j], solution[i]
					score = s2 - (length(points[solution[i-1]], points[solution[i]]) + length(points[solution[j]], points[solution[(j+1) % nodeCount]]))
					solution[i], solution[j] = solution[j], solution[i]
					if score > 0:
						if (bs is None) or (score > bs):
							bs = score
							b = (0,i,j)
			if bs is None:
				break
			method,i,j = b
			if method == 0:
				solution = solution[:i] + solution[i:j+1][::-1] + solution[j+1:]
			elif method == 1:
				solution[i], solution[j] = solution[j], solution[i]
			elif method == 2:
				s = solution.pop(i)
				if i < j:
					solution.insert(j-1, s)
				else:
					solution.insert(j, s)
			bestScore -= bs
			sys.stderr.write("loop %d: swapping from %d to %d with method %d, new best score is %f\n" % (loop, i, j, method, bestScore))
		except KeyboardInterrupt:
			sys.stderr.write("%d loops done\n" % loop)
			break
	bestScore = sum([length(points[solution[x-1]], points[solution[x]]) for x in xrange(nodeCount)])
	return bestScore, solution

def best_insertions(nodeCount, points):
	"""
	Caller for best_insertions functions
	"""
	return best_insertions_v2(nodeCount, points)
def best_insertions_v2(nodeCount, points):
	"""
	Random insertions
	"""
	from random import sample
	sys.stderr.write("starting best_insertions for nodeCount=%d\n" % (nodeCount))
	def total_length(solution):
		return sum([length(points[solution[x-1]], points[solution[x]]) for x in xrange(len(solution))])
	best = range(nodeCount)
	bestScore = total_length(best)
	sys.stderr.write("initial score is %f\n" % bestScore)
	loop, n = 0, nodeCount
	while 1:
		try:
			loop += 1
			nodes = sample(best, n)
			m = max(0, 3 - (nodeCount - n))
			solution = [x for x in best if x not in nodes] + nodes[:m]
			solutionScore = total_length(solution)
			for i in nodes[m:]:
				# insert i in the tour
				bs = None
				for j in xrange(len(solution)):
					score = -length(points[solution[j-1]], points[solution[j]]) + length(points[solution[j-1]], points[i]) + length(points[i], points[solution[j]])
					if (bs is None) or (score < bs):
						bs = score
						b = j
				solutionScore += bs
				if (bestScore is not None) and (solutionScore > bestScore):
					break
				solution.insert(b, i)
			if (bestScore is None) or (solutionScore < bestScore):
				sys.stderr.write("loop=%d, n=%d: best score is now %f\n" % (loop, n, solutionScore))
				bestScore, best, n = solutionScore - 1e-8, list(solution), 1
			else:
				n = (n % nodeCount) + 1
		except KeyboardInterrupt:
			bestScore = total_length(best)
			sys.stderr.write("stopped on loop %d, bestScore=%f\n" % (loop, bestScore))
			break
	return bestScore, best
def best_insertions_v1(nodeCount, points, max_loops=12):
	"""
	Random insertions
	"""
	from random import sample, shuffle
	from itertools import permutations
	sys.stderr.write("starting best_insertions_v1 for nodeCount=%d and max_loops=%d\n" % (nodeCount, max_loops))
	def total_length(solution):
		return sum([length(points[solution[x-1]], points[solution[x]]) for x in xrange(len(solution))])
	def get_order(nodes, max_loops, usePermutations=False):
		if usePermutations:
			for loop, perm in enumerate(permutations(nodes)):
				yield loop, list(perm)
		else:
			for loop in xrange(max_loops):
				shuffle(nodes)
				yield loop, nodes
	best = range(nodeCount)
	bestScore = total_length(best)
	sys.stderr.write("initial score is %f\n" % bestScore)
	i = 1
	while math.factorial(i) <= max_loops:
		i += 1
	k = i - 1
	n = nodeCount
	while 1:
		try:
			nodes = sample(best, n)
			m = max(0, 3 - (nodeCount - n))
			baseSolution = [x for x in best if x not in nodes]
			change = False
			for loop, perm in get_order(nodes, max_loops, n <= k):
				solution = baseSolution + perm[:m]
				solutionScore = total_length(solution)
				for i in perm[m:]:
					# insert i in the tour
					bs = None
					for j in xrange(len(solution)):
						score = -length(points[solution[j-1]], points[solution[j]]) + length(points[solution[j-1]], points[i]) + length(points[i], points[solution[j]])
						if (bs is None) or (score < bs):
							bs = score
							b = j
					solutionScore += bs
					if (bestScore is not None) and (solutionScore > bestScore):
						break
					solution.insert(b, i)
				if (bestScore is None) or (solutionScore < bestScore):
					bestScore, best, change = solutionScore - 1e-8, list(solution), True
					sys.stderr.write("n=%d, loop=%d: best score is now %f\n" % (n, loop, bestScore))
			if change:
				n = 1
			else:
				n = (n % nodeCount) + 1
		except KeyboardInterrupt:
			break
	return total_length(best), best

def fast_swaps(nodeCount, points, bestScore, solution):
	"""
	Improve solution using path swaps.
	"""
	sys.stderr.write("starting fast_swaps for nodeCount=%d and bestScore=%f\n" % (nodeCount, bestScore))
	loop = i = 0
	while 1:
		try:
			loop += 1
			bs = None
			i0 = max(i-1, 0)
			for i in xrange(i0, nodeCount-1):
				s1 = length(points[solution[i-1]], points[solution[i]])
				for j in xrange(i+1, nodeCount-2+min(2, i)):
					s2 = s1 + length(points[solution[j]], points[solution[(j+1) % nodeCount]])
					solution[i], solution[j] = solution[j], solution[i]
					score = s2 - (length(points[solution[i-1]], points[solution[i]]) + length(points[solution[j]], points[solution[(j+1) % nodeCount]]))
					solution[i], solution[j] = solution[j], solution[i]
					if score > 0:
						if (bs is None) or (score > bs):
							bs = score
							b = (i,j)
				if bs is not None:
					i,j = b
					solution = solution[:i] + solution[i:j+1][::-1] + solution[j+1:]
					bestScore -= bs
					# sys.stderr.write("loop %d: swapping from %d to %d, new best score is %f\n" % (loop, i, j, bestScore))
					break
			if bs is None:
				if i0 == 0:
					break
				else:
					i = 0
		except KeyboardInterrupt:
			sys.stderr.write("%d loops done\n" % loop)
			break
	bestScore = sum([length(points[solution[x-1]], points[solution[x]]) for x in xrange(nodeCount)])
	return bestScore, solution
def fast_reinsertions(nodeCount, points, bestScore, solution):
	"""
	Improve solution using reinsertions
	"""
	sys.stderr.write("starting fast_reinsertions for nodeCount=%d and bestScore=%f\n" % (nodeCount, bestScore))
	loop = i = 0
	while 1:
		try:
			loop += 1
			bs = None
			for i in xrange(nodeCount):
				pi = points[solution[i]]
				exclude = set([i, (i+1) % nodeCount, (nodeCount+i+1) % nodeCount])
				d = length(pi, points[solution[i-1]]) + length(pi, points[solution[(i+1) % nodeCount]]) - length(points[solution[i-1]], points[solution[(i+1) % nodeCount]])
				for j in [x for x in xrange(nodeCount) if x not in exclude]:
					score = length(pi, points[solution[j]]) + length(pi, points[solution[j-1]]) - length(points[solution[j-1]], points[solution[j]])
					if score < d:
						score = d - score
						if (bs is None) or (score > bs):
							bs = score
							b = (i,j)
				if bs is not None:
					i, j = b
					s = solution.pop(i)
					if i < j:
						solution.insert(j-1, s)
					else:
						solution.insert(j, s)
					bestScore -= bs
					# sys.stderr.write("loop %d: inserting %d to %d, new best score is %f\n" % (loop, i, j, bestScore))
					break
			if bs is None:
				break
		except KeyboardInterrupt:
			sys.stderr.write("%d loops done\n" % loop)
			break
	bestScore = sum([length(points[solution[x-1]], points[solution[x]]) for x in xrange(nodeCount)])
	return bestScore, solution

if __name__ == '__main__':
	print "Usage: python solver.py /data/tsp_x_x"