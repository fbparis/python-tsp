#!/usr/bin/python
# -*- coding: utf-8 -*-

# This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/.

import numpy as np
import scipy.spatial as spt
import sys
import tsplib95
from random import sample, shuffle
from hashlib import md5
from plot import showpath
from multiprocessing import Pool
from time import sleep

def total_length(solution):
	"""
	Return the total distance (score) of a tour
	"""
	return sum([D[solution[x-1], solution[x]] for x in xrange(len(solution))])
def footprint(solution):
	"""
	Used to find identical solutions.
	"""
	S = solution[solution.index(0):] + solution[:solution.index(0)]
	if S[1] > S[-1]:
		S[1:] = S[1:][::-1]
	return md5("-".join(map(str, S))).hexdigest()
def reset_solution(score, bestScore, unchangedSince, reset_count):
	"""
	Return True if we stop trying to improve the solution.
	"""
	if bestScore == score:
		return False
	return unchangedSince > reset_count * (bestScore / score)**2

def random_insertions(nodeCount, max_loops=1):
	"""
	Generate a solution using best insertions in random order.
	Set max_loops to:
		* 0		: returning a random solution (a bad one)
		* n > 0	: looping n times
	"""
	bestScore = None
	nodes = range(nodeCount)
	loop = 0
	while 1:
		shuffle(nodes)
		if loop >= max_loops:
			break
		loop += 1
		solution = nodes[:3]
		solutionScore = total_length(solution)
		for i in nodes[3:]:
			# insert i in the tour
			R = D[solution[-1:]+solution[:-1],i] + D[solution,i] - D[solution,solution[-1:]+solution[:-1]]
			solutionScore += np.amin(R)
			if (bestScore is not None) and (solutionScore > bestScore):
				break
			solution.insert(np.argmin(R), i)
		if (bestScore is None) or (solutionScore < bestScore):
			solution, solutionScore = fast_swaps(nodeCount, solution, solutionScore)
			bestScore, best = solutionScore - 1e-8, list(solution)
	if bestScore is not None:
		return best
	return nodes

def best_insertions(nodeCount, best, bestScore, globalBestScore):
	"""
	Produce a good / optimal tour pretty quickly.
	Can be used to improve an existing tour or create a new one inserting cities in the tour in random order.
	Insertions in a tour are always made to minimize the cost function (total tour length).
	On first and last round, the solution is passed to another algorithm which will essentially try to swap some cities and remove any crossing edges.
	Search for a better tour then works as follow:
		- starting from n=1, remove n random cities from the tour and reinsert them
		- if the tour is improved, restart from n=1, else n=n+1
		- if n==nodeCount * 0.8 then exit
	"""
	n, nMax, hasChanged = 1, nodeCount * 0.8, False
	while 1:
		nodes = sample(best, n)
		solution = [x for x in best if x not in nodes]
		solutionScore = total_length(solution)
		for i in nodes:
			# insert i in the tour
			R = D[solution[-1:]+solution[:-1],i] + D[solution,i] - D[solution,solution[-1:]+solution[:-1]]
			solutionScore += np.amin(R)
			if solutionScore > bestScore:
				break
			solution.insert(np.argmin(R), i)
		if solutionScore < bestScore:
			# bestScore, best, n, hasChanged = solutionScore - 1e-8, list(solution), n - 1, True
			bestScore, best, n, hasChanged = solutionScore - 1e-8, list(solution), 0, True
			if solutionScore < globalBestScore:
				sys.stderr.write("local bestScore=%f\n" % solutionScore)
		if n >= nMax:
			if hasChanged:
				solution, solutionScore = fast_swaps(nodeCount, best, bestScore)
				if solutionScore < bestScore:
					bestScore, best = solutionScore - 1e-8, list(solution)
			break
		n += 1
	return (best, bestScore, hasChanged)

def fast_swaps(nodeCount, solution, bestScore):
	"""
	Improve solution using path swaps.
	"""
	bestScore += 1e-8
	loop = i = score = 0
	while 1:
		loop += 1
		i0 = max(i-1, 0)
		S1a = D[solution[:-1], solution[-1:]+solution[:-2]]
		S2a = D[solution[1:], solution[2:]+solution[:1]]
		for i in xrange(i0, nodeCount-1):
			A = S1a[i] + S2a[i:]
			S1b = D[solution[i-1], solution[i+1:]]
			S2b = D[solution[i], solution[i+2:]+solution[:1]]
			B = S1b + S2b
			R = (A - B)[:nodeCount - i - 3 + min(2, i)]
			score = np.amax(R)
			if score > 0:
				j = i + 1 + np.argmax(R)
				solution[i:j+1] = solution[i:j+1][::-1]
				bestScore -= score
				break
		if score <= 0:
			if i0 == 0:
				break
			else:
				i = 0
	return solution, bestScore

class Worker(object):
	"""
	A worker class
	"""
	def __init__(self, pool, nodeCount, initialization_loops=0):
		self.pool = pool
		self.nodeCount = nodeCount
		self.initialization_loops = initialization_loops
		self.reset_count = 0
		self.reset()
	def reset(self, inc=1):
		self.isReady = False
		self.reset_count += inc
		self.unchangedSince = 0
		self.footprint = None
		self.result = self.pool.apply_async(random_insertions, [self.nodeCount, self.initialization_loops], callback=self.reset_callback)
	def reset_callback(self, solution):
		self.solution = solution
		self.score = total_length(solution) - 1e-8
		self.isReady = True
	def run(self, globalBestScore):
		self.isReady = False
		self.result = self.pool.apply_async(best_insertions, [self.nodeCount, self.solution, self.score, globalBestScore], callback=self.run_callback)
	def run_callback(self, result):
		self.solution, self.score, hasChanged = result
		if hasChanged:
			self.unchangedSince = 0
		else:
			self.unchangedSince += 1
		self.isReady = True

if __name__ == '__main__':
	if len(sys.argv) > 1:
		name = sys.argv[1].strip()
	else:
		name = sample([x[0] for x in tsplib95.instances if x[1] < 5000], 1)[0]
		sys.stderr.write("random instance chosen: %s\n" % name)
	nodeCount, points, opt = tsplib95.load(name)
	sys.stderr.write("optimal solution should be around %f\n" % opt)
	sys.stderr.write("computing distance matrix for nodeCount=%d..." % nodeCount)
	D = spt.distance.squareform(spt.distance.pdist(np.array(points), "euclidean"))
	sys.stderr.write("done\n")
	initialization_loops = (0, 1, 50, 100)
	pool = Pool(len(initialization_loops))
	workers = []
	for n in initialization_loops:
		workers.append(Worker(pool, nodeCount, n))
	footprints = set()
	solution, score, globalBestScore = None, None, 0.0
	while 1:
		try:
			for worker in workers:
				if worker.isReady:
					if worker.unchangedSince == 0:
						footprints.discard(worker.footprint)
						worker.footprint = footprint(worker.solution)
						if worker.footprint in footprints:
							sys.stderr.write("reseting duplicate worker-%d (score=%f)\n" % (worker.initialization_loops, worker.score))
							worker.reset(0)
							continue
					if (score is None) or (worker.score < score - 1e-8):
						solution, score = list(worker.solution), worker.score
						if (globalBestScore > 0) or (len(footprints) == len(workers) - 1):
							globalBestScore = score
						sys.stderr.write("worker-%d: new best score is %f\n" % (worker.initialization_loops, score))
					if reset_solution(worker.score, score, worker.unchangedSince, worker.reset_count):
						sys.stderr.write("reseting worker-%d (score=%f) after %d loops without improvement\n" % (worker.initialization_loops, worker.score, worker.unchangedSince))
						footprints.remove(worker.footprint)
						worker.reset()
						continue
					footprints.add(worker.footprint)
					worker.run(globalBestScore)				
			sleep(1)
		except KeyboardInterrupt:
			sys.stderr.write("terminating processes...\n")
			pool.terminate()
			pool.join()
			break
	showpath([points[x] for x in solution])
	print total_length(solution), solution
