#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys, tsplib95
from fbtspeed import tsp
from random import choice

def solveIt(name):
	for n, c in tsplib95.instances:
		if name == n:
			nodeCount, points, optimal = tsplib95.load(n)
			sys.stderr.write("optimal score is %f\n" % optimal)
			return tsp(nodeCount, points)
	return "%s instance not found" % name

if __name__ == "__main__":
	if len(sys.argv) > 1:
	    name = sys.argv[1].strip()
	else:
		name = choice([x[0] for x in tsplib95.instances if x[1] < 5000])
		sys.stderr.write("random instance chosen: %s\n" % name)
	print solveIt(name)
