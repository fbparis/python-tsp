# Python TSP

*A Python test suite for Traveling Salesman Problem*

---

Works on Python Anaconda (CMB).

Help yourself.

---

This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/.