#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys, os, tsplib95
import numpy as np
import scipy.spatial as spt
from random import choice
from matplotlib.pyplot import quiver, show

def showpath(path):
	"""
	Graph a TSP tour
	"""
	X = list()
	Y = list()
	DX = list()
	DY = list()
	for i in xrange(len(path)):
		x, y = path[i-1]
		X.append(x)
		Y.append(y)
		dx, dy = path[i]
		dx -= x
		dy -= y
		DX.append(dx)
		DY.append(dy)
	quiver(X, Y, DX, DY, scale_units='xy', angles='xy', scale=1)
	show()
def total_length(tour):
	"""
	Return the total distance (score) of a tour
	"""
	return DistanceMatrix[tour, tour[1:] + tour[:1]].sum()
def k_insertions(nodeCount):
	"""
	Improve a tour by removing and reinserting k nodes in random order:
	- Starting with k=1, k will increase over failed attempts
	- Sample nodes to remove are chosen with weighted random:
		- Nodes with a more dense neighbourhood are more likely to be chosen
	"""
	sys.stderr.write("starting k_insertions for nodeCount=%d\n" % (nodeCount))
	nodes = range(nodeCount)
	# Nodes with a more dense neighbourhood will have higher probability to be challenged
	P = np.square(np.square(DistanceMatrix[nodes]).sum(axis=0))
	P = P.sum() / P
	P = P / P.sum()
	best = list(nodes)
	bestScore = total_length(best) - 1e-8
	# Progressive replacements, starting with 1 to nodeCount
	k, K = 0, range(1, nodeCount)
	resets, steps = 0, nodeCount
	kmax = len(K) * steps
	loop = 0
	while 1:
		try:
			loop += 1
			n = K[(k / steps)]
			if n == 1:
				s = [k]
			else:
				s = np.random.choice(nodes, size=n, replace=False, p=P)
			tour = [x for x in best if x not in s]
			score = total_length(tour)
			for node in s:
				i = np.argmin(DistanceMatrix[tour[-1:] + tour[:-1], node] + DistanceMatrix[tour, node] - DistanceMatrix[tour, tour[-1:] + tour[:-1]])
				score += DistanceMatrix[tour[i - 1], node] + DistanceMatrix[tour[i], node] - DistanceMatrix[tour[i - 1], tour[i]]
				if score >= bestScore:
					break
				tour.insert(i, node)
			if score < bestScore:
				# We use a very fast 2-opt method to increase convergence speed (optional)
				tour = fast_swaps(tour)
				score = total_length(tour)
				sys.stderr.write("loop %d, resets=%d, n=%d: best score is %.2f\n" % (loop, resets, n, score))
				best, bestScore = list(tour), score - 1e-8
				k, resets = 0, 0
			else:
				k = (k + 1) % kmax
				if k == 0:
					resets += 1
					k += resets * steps
					if k >= kmax:
						break
		except KeyboardInterrupt:
			sys.stderr.write("exiting on loop %d, resets=%d, n=%d\n" % (loop, resets, n))
			break
	return best			
def fast_swaps(tour):
	"""
	Improve tour using path swaps.
	"""
	bestScore = total_length(tour)
	nodeCount = len(tour)
	# sys.stderr.write("starting fast_swaps for nodeCount=%d and bestScore=%f\n" % (nodeCount, bestScore))
	loop = i = score = 0
	while 1:
		try:
			loop += 1
			i0 = max(i-1, 0)
			S1a = DistanceMatrix[tour[:-1], tour[-1:]+tour[:-2]]
			S2a = DistanceMatrix[tour[1:], tour[2:]+tour[:1]]
			for i in xrange(i0, nodeCount-1):
				A = S1a[i] + S2a[i:]
				S1b = DistanceMatrix[tour[i-1], tour[i+1:]]
				S2b = DistanceMatrix[tour[i], tour[i+2:]+tour[:1]]
				B = S1b + S2b
				R = (A - B)[:nodeCount - i - 3 + min(2, i)]
				score = np.amax(R)
				if score > 0:
					j = i + 1 + np.argmax(R)
					tour[i:j+1] = tour[i:j+1][::-1]
					bestScore -= score
					break
			if score <= 0:
				if i0 == 0:
					break
				else:
					i = 0
		except KeyboardInterrupt:
			break
	# sys.stderr.write("%d loops done, bestScore=%f\n" % (loop, bestScore))
	return tour

if __name__ == '__main__':
	if len(sys.argv) > 1:
		name = os.path.splitext(os.path.basename(sys.argv[1].strip()))[0]
	else:
		name = choice([x[0] for x in tsplib95.instances if x[1] < 5000])
		sys.stderr.write("random instance chosen: %s\n" % name)
	nodeCount, points, opt = tsplib95.load(name)
	sys.stderr.write("optimal solution should be around %f\n" % opt)
	sys.stderr.write("computing distance matrix for nodeCount=%d..." % nodeCount)
	DistanceMatrix = spt.distance.squareform(spt.distance.pdist(np.array(points), "euclidean"))
	sys.stderr.write("done\n")
	tour = k_insertions(nodeCount)
	score = total_length(tour)
	showpath([points[x] for x in tour])
	print score, tour
