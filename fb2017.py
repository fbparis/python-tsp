#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys, os, tsplib95
import numpy as np
import scipy.spatial as spt
from numpy.random import choice as sample
from plot import showpath

def total_length(solution, D):
	"""
	Return the total distance (score) of a tour
	"""
	return sum([D[solution[x-1], solution[x]] for x in xrange(len(solution))])

def solve_tsp(nodeCount, D):
	"""
	Produce a good / optimal tour pretty quickly.
	Insertions in a tour are always made to minimize the cost function (total tour length).
	Search for a better tour works as follow:
		- we start from tour 0, 1, ..., N
		- we optimize the initial tour with a fast 2-opt deterministic function
		- starting with n = N / 2, we then loop as follow:
			- remove n cities from tour and reinsert them in random order
			- if tour is improved then improve the tour with the fast 2-opt function
			- choose a new n randomly
	"""
	sys.stderr.write("starting solve_tsp for nodeCount=%d\n" % (nodeCount))
	best = fast_swaps(nodeCount, D, range(nodeCount))
	bestScore = total_length(best, D)
	sys.stderr.write("initial score is %f\n" % bestScore)
	loop = 0
	Rn = range(3, nodeCount)
	Pn = np.array([max(1.0, nodeCount / 2.0 - i) for i in xrange(len(Rn))], dtype=float)
	n = Rn[len(Rn) / 2]
	attempts, resets_count = 0, 0
	current, currentScore = list(best), bestScore
	while 1:
		try:
			loop += 1
			nodes = sample(current, n, replace=False)
			solution = [x for x in current if x not in nodes]
			solutionScore = total_length(solution, D)
			stopOnFailure = attempts < nodeCount
			for i in nodes:
				# insert i in the tour
				R = D[solution[-1:]+solution[:-1],i] + D[solution,i] - D[solution,solution[-1:]+solution[:-1]]
				solutionScore += np.amin(R)
				if stopOnFailure and (solutionScore > currentScore):
					break
				solution.insert(np.argmin(R), i)
			if solutionScore < currentScore:
				Pn[n - 3] += 1.0
				attempts = 0
				if solutionScore < bestScore:
					best = fast_swaps(nodeCount, D, solution)
					bestScore = total_length(best, D) - 1e-8
					sys.stderr.write("loop=%d, n=%d, resets_count=%d: best score is now %f\n" % (loop, n, resets_count, bestScore))
					current, currentScore = list(best), bestScore - 1e-8
				else:
					current, currentScore = list(solution), solutionScore - 1e-8
			else:
				Pn[n - 3] /= np.log(n)
				if not stopOnFailure:
					current, currentScore = list(solution), solutionScore - 1e-8
					attempts = 0
					resets_count += 1
				else:
					attempts += 1
			n = sample(Rn, 1, p=Pn / Pn.sum())
		except KeyboardInterrupt:
			sys.stderr.write("stopped on loop %d, resets_count=%d, bestScore=%f\n" % (loop, resets_count, bestScore))
			break
	return best

def fast_swaps(nodeCount, D, solution=None):
	"""
	Improve solution using path swaps.
	"""
	if solution is None:
		solution = range(nodeCount)
	bestScore = total_length(solution, D)
	sys.stderr.write("starting fast_swaps for nodeCount=%d and bestScore=%f\n" % (nodeCount, bestScore))
	loop = i = score = 0
	while 1:
		try:
			loop += 1
			i0 = max(i-1, 0)
			S1a = D[solution[:-1], solution[-1:]+solution[:-2]]
			S2a = D[solution[1:], solution[2:]+solution[:1]]
			for i in xrange(i0, nodeCount-1):
				A = S1a[i] + S2a[i:]
				S1b = D[solution[i-1], solution[i+1:]]
				S2b = D[solution[i], solution[i+2:]+solution[:1]]
				B = S1b + S2b
				R = (A - B)[:nodeCount - i - 3 + min(2, i)]
				score = np.amax(R)
				if score > 0:
					j = i + 1 + np.argmax(R)
					solution[i:j+1] = solution[i:j+1][::-1]
					bestScore -= score
					# sys.stderr.write("loop %d: swapping from %d to %d, new best score is %f\n" % (loop, i, j, bestScore))
					break
			if score <= 0:
				if i0 == 0:
					break
				else:
					i = 0
		except KeyboardInterrupt:
			break
	sys.stderr.write("%d loops done, bestScore=%f\n" % (loop, bestScore))
	return solution

if __name__ == '__main__':
	if len(sys.argv) > 1:
		name = os.path.splitext(os.path.basename(sys.argv[1].strip()))[0]
	else:
		name = sample([x[0] for x in tsplib95.instances if x[1] < 5000], 1)[0]
		sys.stderr.write("random instance chosen: %s\n" % name)
	nodeCount, points, opt = tsplib95.load(name)
	sys.stderr.write("optimal solution should be around %f\n" % opt)
	sys.stderr.write("computing distance matrix for nodeCount=%d..." % nodeCount)
	D = spt.distance.squareform(spt.distance.pdist(np.array(points), "euclidean"))
	sys.stderr.write("done\n")
	solution = solve_tsp(nodeCount, D)
	score = total_length(solution, D)
	showpath([points[x] for x in solution])
	print score, solution
