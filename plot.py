#!/usr/bin/python
# -*- coding: utf-8 -*-

from matplotlib.pyplot import quiver, show

def showpath(path):
	X = list()
	Y = list()
	DX = list()
	DY = list()
	for i in xrange(len(path)):
		x, y = path[i-1]
		X.append(x)
		Y.append(y)
		dx, dy = path[i]
		dx -= x
		dy -= y
		DX.append(dx)
		DY.append(dy)
	quiver(X, Y, DX, DY, scale_units='xy', angles='xy', scale=1)
	show()